#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>

set -e
set -u
set -x
trap 'exit 128' INT

exec docker run --interactive --rm --tty --name xgqt-pingpulse-0 \
     -p 5000:5000 docker.io/xgqt/pingpulse:latest "${@}"

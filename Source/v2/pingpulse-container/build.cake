var target = Argument("target", "build");
var configuration = Argument("configuration", "Release");

Task("clean")
  .Does(() => {
    StartProcess("dotnet", new ProcessSettings {
        Arguments =
        "clean --verbosity quiet " +
        "../v2.sln"
      });
  });

Task("restore")
  .Does(() => {
    StartProcess("dotnet", new ProcessSettings {
        Arguments =
        "restore --force --force-evaluate --verbosity quiet " +
        "--runtime linux-x64 " +
        "../v2.sln"
      });
  });

Task("build")
  .IsDependentOn("clean")
  .IsDependentOn("restore")
  .Does(() => {
    StartProcess("dotnet", new ProcessSettings {
        Arguments =
        $"build --configuration {configuration} --no-restore " +
        $"--output /Build_v2/pingpulse " +
        "../pingpulse-app/src/PingPulse.App"
      });

    StartProcess("dotnet", new ProcessSettings {
        Arguments =
        $"publish --configuration {configuration} --no-restore " +
        "--self-contained -p:PublishSingleFile=true " +
        $"--output /Build_v2/tinystatus-fs " +
        "../tinystatus-fs-app/src/TinyStatus.App"
      });
  });

RunTarget(target);

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>

open System
open System.IO
open Fli

let title = "Tinystatus"
let header = "Global status"
let timeout = 10

let commandExists (command: string) =
    cli {
        Exec "which"
        Arguments [| command |]
    }
    |> Command.execute
    |> Output.toExitCode = 0

let getCsvColumn (column: int) (csvRow: string) =
    (csvRow.Split(",") |> Array.map (fun s -> s.Trim()))[column]

let doCheck (checkType: string) (host: string) (expectation: string) =
    let ipVersion =
        match checkType with
        | v6 when v6.EndsWith("6") -> 6
        | _ -> 4

    match checkType with
    | http when http.StartsWith("http") ->
        cli {
            Exec "curl"

            Arguments
                [| $"-{ipVersion}"
                   "-L"
                   "-S"
                   "-k"
                   "-l"
                   "-s"
                   "-o"
                   "/dev/null"
                   "-m"
                   $"{timeout}"
                   "-w"
                   "%{http_code}"
                   host |]
        }
        |> Command.execute
        |> Output.toText = expectation
    | ping when ping.StartsWith("ping") ->
        cli {
            Exec "ping"
            Arguments [| $"-{ipVersion}"; "-W"; $"{timeout}"; "-c"; "1"; host |]
        }
        |> Command.execute
        |> Output.toExitCode = (expectation |> int)
    | port when port.StartsWith("port") ->
        cli {
            Exec "nc"
            Arguments [| $"-{ipVersion}"; "-w"; $"{timeout}"; "-z"; "-v"; host |]
        }
        |> Command.execute
        |> Output.toExitCode = (expectation |> int)
    | directive -> $"unknown directive {directive}" |> Exception |> raise

[<EntryPoint>]
let main args =
    let checksFile = if args.Length >= 1 then args[0] else "./checks.csv"
    // let incidentsFile = if args.Length >= 2 then args[1] else "./incidents.txt"

    let requiredCommands = [| "curl"; "nc"; "ping" |]

    for requiredCommand in requiredCommands do
        if not (commandExists requiredCommand) then
            $"required command {requiredCommand} was not found" |> Exception |> raise

    let statuses =
        let contents =
            if File.Exists checksFile then
                checksFile
                |> File.ReadAllLines
            else
                [||]

        contents
        |> Array.filter (fun s -> not (String.IsNullOrEmpty s))
        |> Array.map (fun rowString ->
            let checkType = rowString |> getCsvColumn 0
            let expectation = rowString |> getCsvColumn 1
            let name = rowString |> getCsvColumn 2
            let host = rowString |> getCsvColumn 3

            let hostLink =
                match checkType with
                | s when s.StartsWith("http") -> $"<a href='{host}'>{host}</a>"
                | _ -> host

            let checkPass = doCheck checkType host expectation

            if checkPass then
                $"<li>{name} <small>{hostLink}</small> <span class='status success'>Operational</span></li>"
            else
                $"<li>{name} <small>{hostLink}</small> <span class='small failed'></span><span class='status failed'>Disrupted</span></li>"
            )
        |> String.Concat

    let htmlStyle =
        """
body { font-family: segoe ui,Roboto,Oxygen-Sans,Ubuntu,Cantarell,helvetica neue,Verdana,sans-serif; }
h1 { margin-top: 30px; }
ul { padding: 0px; }
li { list-style: none; margin-bottom: 2px; padding: 5px; border-bottom: 1px solid #ddd;  }
small { color: #6c757d; }
small a { color: #6c757d !important; }
.container { max-width: 600px; width: 100%; margin: 15px auto; }
.panel { text-align: center; padding: 10px; border: 0px; border-radius: 5px; }
.failed-bg  { color: white; background-color: #E25D6A; }
.success-bg { color: white; background-color: #52B86A; }
.failed  { color: #E25D6A; }
.success { color: #52B86A; }
.small { font-size: 80%; }
.status { float: right; }
"""

    let htmlBody =
        $"""<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>{title}</title>
<style>
{htmlStyle}
</style>
</head>
<body>
<div class="container">
<h1>{header}</h1>
<h1>Services</h1>
<ul>
{statuses}
</ul>
<p class="small"> Last check: </p>
<p>No incident reported yet ;)</p>
</div>
</body>
</html>
"""

    printfn "%s" htmlBody

    0

#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>

set -e
set -u
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/..")"
cd "${source_root}"

set -x

make -C pingpulse-container build

docker run --interactive --rm --tty --name xgqt-pingpulse-0 \
       --volume "${source_root}/tinystatus-fs-app/example-config:/pingpulse-data" \
       -p 5000:5000 docker.io/xgqt/pingpulse:latest

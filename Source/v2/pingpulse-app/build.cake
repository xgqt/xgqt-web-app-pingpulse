var target = Argument("target", "build");
var configuration = Argument("configuration", "Release");

Task("clean")
  .Does(() => {
    StartProcess("dotnet", new ProcessSettings {
        Arguments =
        "clean --verbosity quiet " +
        "./src/PingPulse.App/PingPulse.App.fsproj"
      });
  });

Task("restore")
  .Does(() => {
    StartProcess("dotnet", new ProcessSettings {
        Arguments =
        "restore --force-evaluate --verbosity quiet " +
        "./src/PingPulse.App/PingPulse.App.fsproj"
      });
  });

Task("build")
  .IsDependentOn("restore")
  .Does(() => {
    StartProcess("dotnet", new ProcessSettings {
        Arguments =
        $"build --configuration {configuration} --no-restore " +
        "./src/PingPulse.App/PingPulse.App.fsproj"
      });
  });

Task("test")
  .IsDependentOn("build")
  .Does(() => {
    StartProcess("dotnet", new ProcessSettings {
        Arguments =
        $"test --configuration {configuration} " +
        "./src/PingPulse.App/PingPulse.App.fsproj"
      });
  });

RunTarget(target);

// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.
// Copyright (c) 2024, Maciej Barć <xgqt@xgqt.org>

module PingPulse.App.App

open System
open System.IO
open System.Timers

open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Cors.Infrastructure
open Microsoft.AspNetCore.Hosting

open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging
open Microsoft.Extensions.DependencyInjection

open Fli
open Giraffe

let getConfigPath (defaultPath: string) : string =
    (match Environment.GetEnvironmentVariable "PINGPULSE_CONFIG_PATH" with
     | emptyValue when String.IsNullOrEmpty emptyValue -> defaultPath
     | value -> value)
    |> Path.GetFullPath

let getTimerTickSeconds () : float =
    match Environment.GetEnvironmentVariable "PINGPULSE_TIMER_TICK_SECONDS" with
    | emptyValue when String.IsNullOrEmpty emptyValue -> 60.0
    | value -> Double.Parse value

let findTinystatusExe () : string =
    let predefinedSearchPaths: string array = [| "."; "../../../tinystatus-app" |]

    let systemSearchPaths: string array =
        (Environment.GetEnvironmentVariable "PATH").Split ":"

    let maybeFoundDirName: string option =
        Array.append predefinedSearchPaths systemSearchPaths
        |> Seq.tryFind (fun p -> Path.Combine(p, "tinystatus") |> File.Exists)

    match maybeFoundDirName with
    | Some(dirName) -> Path.Combine(dirName, "tinystatus") |> Path.GetFullPath
    | _ -> $"Could not find the tinystatus executable" |> Exception |> raise

let writeToFile (filePath: string) (contents: string) : unit =
    File.WriteAllText(filePath, contents)

let callTinystatus (configPath: string) (outputDirectory: string) : unit =
    printfn " Running Tinystatus..."

    let tinystatusExe = findTinystatusExe ()

    cli {
        Exec tinystatusExe
        Arguments [ Path.Combine(configPath, "checks.csv");
                    Path.Combine(configPath, "incidents.txt") ]
        WorkingDirectory outputDirectory
    }
    |> Command.execute
    |> Output.throwIfErrored
    |> Output.toText
    |> writeToFile (Path.Combine(outputDirectory, "index.html"))

let webApp =
    choose [
        GET >=>
            choose [
                route "/healthCheck" >=> setStatusCode 200 >=> text "healthy"
            ]
        setStatusCode 404 >=> text "Not Found" ]

let errorHandler (ex : Exception) (logger : ILogger) =
    logger.LogError(ex, "An unhandled exception has occurred while executing the request.")
    clearResponse >=> setStatusCode 500 >=> text ex.Message

let configureCors (builder : CorsPolicyBuilder) =
    builder
        .WithOrigins(
            "http://localhost:5000",
            "https://localhost:5001")
       .AllowAnyMethod()
       .AllowAnyHeader()
       |> ignore

let configureApp (app : IApplicationBuilder) =
    let env = app.ApplicationServices.GetService<IWebHostEnvironment>()
    (match env.IsDevelopment() with
    | true  ->
        app.UseDeveloperExceptionPage()
    | false ->
        app .UseGiraffeErrorHandler(errorHandler)
            .UseHttpsRedirection())
        .UseCors(configureCors)
        .UseStaticFiles()
        .UseGiraffe(webApp)

let configureServices (services : IServiceCollection) =
    services.AddCors()    |> ignore
    services.AddGiraffe() |> ignore

let configureLogging (builder : ILoggingBuilder) =
    builder.AddConsole()
           .AddDebug() |> ignore

[<EntryPoint>]
let main args =
    let contentRoot = Directory.GetCurrentDirectory()
    let webRoot     = Path.Combine(contentRoot, "WebRoot")

    let configPath = getConfigPath contentRoot
    let timerTickSeconds = getTimerTickSeconds ()

    printfn " Content root: %s" contentRoot
    printfn " Web root: %s" webRoot

    printfn " Config path: %s" configPath
    printfn " Timer tick seconds: %f" timerTickSeconds

    let timerBody () = callTinystatus configPath webRoot

    timerBody ()

    let timer = new Timer(timerTickSeconds * 1000.0)

    timer.Elapsed.Add(fun _elapsedEventArgs -> timerBody ())
    timer.AutoReset <- true
    timer.Enabled <- true

    Host.CreateDefaultBuilder(args)
        .ConfigureWebHostDefaults(
            fun webHostBuilder ->
                webHostBuilder
                    .UseContentRoot(contentRoot)
                    .UseWebRoot(webRoot)
                    .Configure(Action<IApplicationBuilder> configureApp)
                    .ConfigureServices(configureServices)
                    .ConfigureLogging(configureLogging)
                    |> ignore)
        .Build()
        .Run()

    0

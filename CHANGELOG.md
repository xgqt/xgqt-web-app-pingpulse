## Unreleased

## 2.0.0 (2024-05-26)

### Feat

- **pingpulse-container**: add run.sh
- **v2**: port v1 projects to new v2
- **tinystatus-fs-app**: port script to program
- **tinystatus-fs-app**: add initial port of tinystatus to f# script
- **pingpulse-container/Containerfile**: add PINGPULSE_PORT env var
- **pingpulse-container**: add pingpulse-container

### Fix

- **pingpulse-container/build.cake**: fix publish for tinystatus-fs
- **pingpulse-container/Containerfile**: use aspnet base
- **pingpulse-container/Containerfile**: remove PINGPULSE_PORT, use ASPNETCORE_URLS

### Refactor

- **pingpulse-container/Containerfile**: remove misleading EXPOSE
- **PingPulse.App/WebRoot**: remove unneeded main.css

## 1.0.1 (2024-05-17)

### Feat

- **PingPulse.App/Program.fs**: allow config path and tick seconds to be configured
- **PingPulse.App/Program.fs**: implement timer calling tinystatus
- **PingPulse.App/Program.fs**: add callTinystatus
- **PingPulse.App/Program.fs**: add findTinystatusExe
- **pingpulse-app**: add pingpulse-app project
- **tinystatus-app**: add vendored tinystatus

### Refactor

- **PingPulse.App/Program.fs**: add copyright header
- **PingPulse.App/Program.fs**: cleanup; use html from webroot
